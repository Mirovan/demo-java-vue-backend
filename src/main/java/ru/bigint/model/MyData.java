package ru.bigint.model;

public class MyData {
    private int intField;
    private String stringField;

    public int getIntField() {
        return intField;
    }

    public void setIntField(int intField) {
        this.intField = intField;
    }

    public String getStringField() {
        return stringField;
    }

    public void setStringField(String stringField) {
        this.stringField = stringField;
    }

    public MyData(int intField, String stringField) {
        this.intField = intField;
        this.stringField = stringField;
    }
}
