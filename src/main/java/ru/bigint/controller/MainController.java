package ru.bigint.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.bigint.model.MyData;

@RestController
@RequestMapping("/api/")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class MainController {

    @GetMapping("simple-data/")
    public String simpleData() {
        return "sample data from rest";
    }

    @GetMapping("list/")
    public String[] list() {
        String[] arr = {"aaa", "bbb", "abc"};
        return arr;
    }

    @GetMapping("object/")
    public MyData object() {
        return new MyData(123, "String field");
    }
}